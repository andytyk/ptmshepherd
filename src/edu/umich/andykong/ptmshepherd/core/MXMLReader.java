package edu.umich.andykong.ptmshepherd.core;

import java.io.*;
import java.util.*;

import umich.ms.datatypes.LCMSDataSubset;
import umich.ms.datatypes.index.IndexElement;
import umich.ms.datatypes.scan.IScan;
import umich.ms.datatypes.scancollection.impl.ScanCollectionDefault;
import umich.ms.datatypes.spectrum.ISpectrum;
import umich.ms.fileio.filetypes.mzml.MZMLFile;
import umich.ms.fileio.filetypes.mzml.MZMLIndex;
import umich.ms.fileio.filetypes.mzml.MZMLIndexElement;
import umich.ms.fileio.filetypes.mzxml.MZXMLFile;
import umich.ms.fileio.filetypes.mzxml.MZXMLIndex;


public class MXMLReader {

	File f;
	
	int nSpecs;
	Spectrum [] specs;
//	HashMap<String,Spectrum> specsByName;
	HashMap<String,Spectrum> specsByStrippedName;
	
	public Spectrum getSpectrum(String specName) {
		return specsByStrippedName.get(stripName(specName));
//		return specsByName.get(specName);
	}
	
	public static String stripName(String specName) {
		String [] sp = specName.split("\\.");
		if(sp.length == 4) 
			return sp[0]+"."+sp[1];
		else
			return specName;
	}
	
	public MXMLReader(File f) {
		this.f = f;
	}
	
	public void readFully() throws Exception {
		if(f.getName().endsWith(".mzXML"))
			readFullyMzXML();
		else if(f.getName().endsWith(".mzML"))
			readFullyMzML();
		else {
			System.out.println("Cannot read mzFile with unrecognized extension: " + f.getName());
			System.exit(1);
		}
//		specsByName = new HashMap<>();
		specsByStrippedName = new HashMap<>();
		for(int i = 0; i < specs.length; i++) {
//			specsByName.put(specs[i].scanName, specs[i]);
			specsByStrippedName.put(stripName(specs[i].scanName), specs[i]);
		}
	}
	
	public void readFullyMzXML() throws Exception {
		MZXMLFile source = new MZXMLFile(f.getAbsolutePath());
		String baseName = f.getName();
		baseName = baseName.substring(0,baseName.indexOf(".mzXML"));
		
		source.setExcludeEmptyScans(false);
		source.setNumThreadsForParsing(Math.min(8,Runtime.getRuntime().availableProcessors()));
		
		ScanCollectionDefault scans = new ScanCollectionDefault();
		scans.setDataSource(source);
		scans.loadData(LCMSDataSubset.MS2_WITH_SPECTRA);
		TreeMap<Integer,IScan> num2scan = scans.getMapNum2scan();
		Set<Map.Entry<Integer, IScan>> scanEntries = num2scan.entrySet();
		
		ArrayList<Spectrum> cspecs = new ArrayList<Spectrum>();
        MZXMLIndex idx = source.fetchIndex();
        for (Map.Entry<Integer, IScan> scanEntry : scanEntries) {
            IScan scan = scanEntry.getValue();

            if(scan.getMsLevel() != 2)
            	continue;
            
            int scanNumInternal = scan.getNum();
            IndexElement idxElem = idx.getByNum(scanNumInternal);
            int scanNumRaw = idxElem.getRawNumber();

            ISpectrum spectrum = scan.fetchSpectrum();
            int clen = (spectrum == null)?0:spectrum.getMZs().length;
            Spectrum ns = new Spectrum(clen);
            try {
            	ns.charge = (byte)scan.getPrecursor().getCharge().intValue();
            } catch(Exception e) {
            	ns.charge = 0;
            }
            ns.scanNum = scanNumRaw;
            ns.rt = scan.getRt();
            ns.scanName = baseName+"."+scanNumRaw+"."+scanNumRaw+"."+ns.charge;
            ns.precursorMass = scan.getPrecursor().getMzTarget();
            for(int i = 0; i < clen; i++) {
            	ns.peakMZ[i] = (float)spectrum.getMZs()[i];
            	ns.peakInt[i] = (float)spectrum.getIntensities()[i];
            }
            cspecs.add(ns);            
        }
        nSpecs = cspecs.size();
        specs = new Spectrum[nSpecs];
        cspecs.toArray(specs);
        scans.reset();
	}
	
	public void readFullyMzML() throws Exception {
		MZMLFile source = new MZMLFile(f.getAbsolutePath());
		String baseName = f.getName();
		baseName = baseName.substring(0,baseName.indexOf(".mzML"));
		
		source.setExcludeEmptyScans(false);
		source.setNumThreadsForParsing(Math.min(8,Runtime.getRuntime().availableProcessors()));
		
		ScanCollectionDefault scans = new ScanCollectionDefault();
		scans.setDataSource(source);
		scans.loadData(LCMSDataSubset.MS2_WITH_SPECTRA);
		TreeMap<Integer,IScan> num2scan = scans.getMapNum2scan();
		Set<Map.Entry<Integer, IScan>> scanEntries = num2scan.entrySet();
		
		ArrayList<Spectrum> cspecs = new ArrayList<Spectrum>();
        MZMLIndex idx = source.fetchIndex();
        for (Map.Entry<Integer, IScan> scanEntry : scanEntries) {
            Integer scanNum = scanEntry.getKey();
            IScan scan = scanEntry.getValue();

            if(scan.getMsLevel() != 2)
            	continue;
            
            MZMLIndexElement idxElem = idx.getByNum(scanNum);
            String id = idxElem.getId();
            
            int scanNumRaw = scanNum;
//            String [] sp = id.split(" ");
//            for(int i = 0; i < sp.length; i++) {
//            	if(sp[i].startsWith("scan="))
//            		scanNumRaw = Integer.parseInt(sp[i].substring(sp[i].indexOf("=")+1));
//            	if(sp[i].startsWith("cycle="))
//            		scanNumRaw = Integer.parseInt(sp[i].substring(sp[i].indexOf("=")+1));
//            }
//            if(scanNumRaw == -1)
//            	scanNumRaw = Integer.parseInt(sp[sp.length-1].substring(sp[sp.length-1].indexOf("=")+1));
            
            ISpectrum spectrum = scan.fetchSpectrum();
            int clen = (spectrum == null)?0:spectrum.getMZs().length;
            Spectrum ns = new Spectrum(clen);
            try {
            	ns.charge = (byte)scan.getPrecursor().getCharge().intValue();
            } catch(Exception e) {
            	ns.charge = 0;
            }
            ns.scanNum = scanNumRaw;
            ns.rt = scan.getRt();
            ns.scanName = baseName+"."+scanNumRaw+"."+scanNumRaw+"."+ns.charge;
            ns.precursorMass = scan.getPrecursor().getMzTarget();
            for(int i = 0; i < clen; i++) {
            	ns.peakMZ[i] = (float)spectrum.getMZs()[i];
            	ns.peakInt[i] = (float)spectrum.getIntensities()[i];
            }
            cspecs.add(ns);            
        }
        nSpecs = cspecs.size();
        specs = new Spectrum[nSpecs];
        cspecs.toArray(specs);
        scans.reset();
	}
	
}
